# meer controle over de output van strings


animal = "cow"
item = "moon"

# print("The " + animal + " jumped over " + item)


# print(" The {} jumped over the {} ".format("Koe", "Maan"))
# print(" The {1} jumped over the {0} ".format(animal, item))   #positional arguments
# print(" The {0} jumped over the {1} ".format(animal, item))   #positional arguments
# print(" The {animal} jumped over the {item} ".format(animal="paard",item="hekje"))   #positional arguments
# print(" The {1} jumped over the {1} ".format(animal, item))   #positional arguments



text = "The {} jumped over the {}"
print(text.format(animal,item))